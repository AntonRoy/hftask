#pragma once

#include <string>
#include <vector>
#include <sstream>

class CSVRow {
public:
    std::string operator[](std::size_t index) const;
    std::size_t size() const;
    void readNextRow(std::istream& str);
private:
    std::string m_line;
    std::vector<int> m_data;
};

std::istream& operator>>(std::istream& str, CSVRow& data);
