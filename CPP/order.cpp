#include "order.h"

Order::Order(const std::string &_orderType, const bool _isBuy, const long double _orderSize,
             const long long int _submitTimestamp) {
    if (_orderType != MARKET_ORDER) {
        throw std::invalid_argument("Invalid order arguments set");
    }
    id = counter++;
    orderType = _orderType;
    orderSize = _orderSize;
    isBuy = _isBuy;
    submitTimestamp = _submitTimestamp;
}

Order::Order(const std::string &_orderType, const bool _isBuy, const long double _orderPrice,
             const long double _orderSize, const long long int _submitTimestamp) {
    if (_orderType != LIMIT_ORDER) {
        throw std::invalid_argument("Invalid order arguments set");
    }
    id = counter++;
    orderType = _orderType;
    orderPrice = _orderPrice;
    orderSize = _orderSize;
    isBuy = _isBuy;
    submitTimestamp = _submitTimestamp;
}

bool Order::IsBuy() const {
    return isBuy;
}

bool Order::IsMarket() const {
    return orderType == MARKET_ORDER;
}

long long Order::GetSubmitTimestamp() const {
    return submitTimestamp;
}

long double Order::GetPrice() const {
    return orderPrice;
}

long double Order::GetSize() const {
    return orderSize;
}

void Order::UpdateLimitQueue(const long double _limitQueue) {
    limitQueue = _limitQueue;
}

long double Order::GetLimitQueue() const {
    return limitQueue;
}

void Order::AddSize(const long double add) {
    orderSize += add;
}

void Order::CanFullFill() {
    canFullFill = true;
}

bool Order::IsCanFullFill() const {
    return canFullFill;
}
