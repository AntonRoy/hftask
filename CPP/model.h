#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <deque>
#include <cmath>

#include "trade.h"
#include "trade_manager.h"
#include "order.h"
#include "order_book.h"
#include "CPP/bar.h"
#include "position.h"
#include "CPP/event.h"
#include "CPP/backtest.h"

class Backtest;

class Model {
public:
    inline static std::string BUY = "buy",
            SELL = "sell",
            BUY_SENT = "buy_sent",
            SELL_SENT = "sell_sent";

    Model(const long double _maxUsdPosition,
          const long long _deltaTime,
          const long long _endOfTradingPeriod,
          const long double _bestPriceDelta);

    long double GetAvg() const;

    long double GetZScore() const;

    bool predict();

    bool predictSel();

    void CancelOrder();

    void CheckUp();

    void SubmitBuy();

    void SubmitSell(const std::string& orderType);

    void OnOrderUpdate(const std::string& updateType);

    void OnDataUpdate(const Bar& bar);

    long double GetBalanceUsd() const;

    void AddBalanceUsd(const double add);
private:
    std::string state; // SELL |  BUY | SELL_SENT | BUY_SENT
    std::vector<Bar> bars;
    std::optional<Order> order;
    std::optional<Position> position;
    long long endOfTradingPeriod, deltaTime;
    long double maxUsdPosition, balanceUsd;
    long double bestPriceDelta;
    long double sizeEps, priceEps;
    long double positionEntryAvgPrice;
};