#include "event.h"

bool Event::operator<(const Event &other) const {
    return timestamp < other.timestamp;
}

long long Event::GetTimestamp() const {
    return timestamp;
}
