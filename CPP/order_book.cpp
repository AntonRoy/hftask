#include "order_book.h"

OrderBook::OrderBook(const CSVRow &row) {
    timestamp = std::stoll(row[1]);

    asks.resize(orderBookSize);
    size_t askStart = 2;
    for (size_t i = askStart; i < askStart + orderBookSize; ++i) {
        asks[i - askStart].price = std::stod(row[i]);
    }
    for (size_t i = askStart + orderBookSize; i < askStart + 2 * orderBookSize; ++i) {
        asks[i - askStart - orderBookSize].volume = std::stod(row[i]);
    }

    bids.resize(orderBookSize);
    size_t bidStart = askStart + 2 * orderBookSize;
    for (size_t i = bidStart; i < bidStart + orderBookSize; ++i) {
        bids[i - bidStart].price = std::stod(row[i]);
    }
    for (size_t i = bidStart + orderBookSize; i < bidStart + 2 * orderBookSize; ++i) {
        bids[i - bidStart - orderBookSize].volume = std::stod(row[i]);
    }
}

long long OrderBook::GetTimestamp() const {
    return timestamp;
}

void OrderBook::print() const {
    std::cout << "timestamp: " << timestamp << "\n";
    std::cout << "asks: ";
    for (const auto& row : asks) {
        std::cout << "(" << row.price << "; " << row.volume << ") ";
    }
    std::cout << "\n";
    std::cout << "bids: ";
    for (const auto& row : bids) {
        std::cout << "(" << row.price << "; " << row.volume << ") ";
    }
    std::cout << "\n";
}

long double OrderBook::GetLimitQueue(const long double price, bool isBuy) const {
    long double queue = 0;
    if (!isBuy) {
        for (size_t i = 0; i < asks.size() && asks[i].price <= price; ++i) {
            queue += asks[i].volume;
        }
    } else {
        for (size_t i = 0; i < bids.size() && bids[i].price >= price; ++i) {
            queue += bids[i].volume;
        }
    }
    return queue;
}

std::vector<OrderBook> readOrderBooks(const std::string &filename) {
    std::ifstream file(filename);
    CSVRow columns, row;
    file >> columns;
    std::vector<OrderBook> orderBooks;
    while (file >> row) {
        orderBooks.push_back(OrderBook(row));
    }
    return orderBooks;
}
