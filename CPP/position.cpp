#include "position.h"

Position::Position(long long int _timestamp, long double _price, long double _size) {
    timestamp = _timestamp;
    price = _price;
    size = _size;
    id = counter++;
}

long double Position::GetPrice() const {
    return price;
}

long double Position::GetSize() const {
    return size;
}

long long Position::GetTimestamp() const {
    return timestamp;
}

void Position::AddSize(const double add) {
    size += add;
}
