#include "trade_manager.h"

TradeManager::TradeManager(const CSVRow &columns) {
    for (size_t i = 1; i < columns.size(); ++i) {
        column2index[columns[i]] = i;
    }
}

Trade TradeManager::makeTrade(const CSVRow &row) {
    long double tradePrice = std::stod(row[column2index["trade_price"]]);
    long double tradeSize = std::stod(row[column2index["trade_size"]]);
    long long tradeTimestamp = std::stoll(row[column2index["last_trade_timestamp"]]);
    bool isBuyerMaker = row[column2index["is_buyer_maker"]] == "True" ? true : false;
    return Trade(tradePrice, tradeSize, tradeTimestamp, isBuyerMaker);
}

std::vector<Trade> readTrades(const std::string &filename) {
    std::ifstream file(filename);
    CSVRow columns, row;
    file >> columns;
    TradeManager tradeManager(columns);
    std::vector<Trade> trades;
    while (file >> row) {
        trades.push_back(tradeManager.makeTrade(row));
    }
    return trades;
}
