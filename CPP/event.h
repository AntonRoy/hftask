#pragma once

#include <string>

#include "order.h"
#include "order_book.h"
#include "trade.h"
#include "CPP/bar.h"

struct Event {
    inline static const std::string CANCEL_ORDER = "cancel_order",
            SUBMIT_ORDER = "submit_order",
            CHECK_UP = "check_up",
            UPDATE_ORDER_BOOK = "update_order_book",
            NEW_TRADE = "new_trade",
            NEW_BAR = "new_bar";

    long long timestamp{};
    std::string type{};
    std::optional<Order> order{};
    OrderBook orderBook{};
    Trade newTrade{};
    Bar newBar{};

//    Event(const std::string& _type,
//          Order* _order,
//          const long long _timestamp)
//            : type(_type), order(_order), timestamp(_timestamp) {
//    }
//    Event(const std::string& _type,
//          const long long _timestamp) : type(_type), timestamp(_timestamp) {
//    }
//    Event(const std::string& _type,
//          OrderBook* _orderBook,
//          const long long _timestamp)
//            : type(_type), orderBook(_orderBook), timestamp(_timestamp) {
//    }
//    Event(const std::string& _type,
//          Trade* _newTrade,
//          const long long _timestamp)
//            : type(_type), newTrade(_newTrade), timestamp(_timestamp) {
//    }
//    Event(const std::string& _type,
//          Bar* _newBar,
//          const long long _timestamp)
//            : type(_type), newBar(_newBar), timestamp(_timestamp) {
//    }

    bool operator<(const Event& other) const;

    long long GetTimestamp() const;
};