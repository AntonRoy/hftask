#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <algorithm>

#include "CPP/trade_manager.h"
#include "CPP/order_book.h"
#include "CPP/bar.h"
#include "CPP/event.h"
#include "CPP/backtest.h"
#include "CPP/model.h"

int main() {
    std::ofstream log("../log.txt");
    log << "Processing order_books..." << std::endl;
    auto orderBooks = readOrderBooks("../Data/order_book.csv");
    log << "order_books processed" << std::endl;
    log << "Processing trades..." << std::endl;
    auto trades = readTrades("../Data/trades.csv");
    log << "trades processed" << std::endl;

    //init backtest
    initBacktest("../Data/backtest_params.txt");

    //init model
    long double modelMaxUsdPosition, modelBestPriceDelta;
    long long timeframe;
    std::ifstream modelParamsFile("../Data/model_params.txt");
    modelParamsFile >> modelMaxUsdPosition >>  timeframe >> modelBestPriceDelta;
    Model model(modelMaxUsdPosition,
                10 * timeframe,
                orderBooks.back().GetTimestamp(),
                modelBestPriceDelta);

    //make events
    std::multiset<Event> events;
    for (auto& orderBook : orderBooks) {
        Event eventCheckUp;
        eventCheckUp.type = Event::CHECK_UP;
        eventCheckUp.timestamp = orderBook.GetTimestamp();
        events.insert(eventCheckUp);
        Event eventUpdateOrderBook;
        eventUpdateOrderBook.type = Event::UPDATE_ORDER_BOOK;
        eventUpdateOrderBook.orderBook = orderBook;
        eventUpdateOrderBook.timestamp = orderBook.GetTimestamp();
        events.insert(eventUpdateOrderBook);
    }
    for (auto& trade : trades) {
        Event eventNewTrade;
        eventNewTrade.type = Event::NEW_TRADE;
        eventNewTrade.newTrade = trade;
        eventNewTrade.timestamp = trade.GetTimestamp();
        events.insert(eventNewTrade);
    }
    std::vector<Bar> bars = CreateBars(trades, timeframe);
    for (const auto& bar : bars) {
        Event eventNewBar;
        eventNewBar.type = Event::NEW_BAR;
        eventNewBar.newBar = bar;
        eventNewBar.timestamp = bar.timestamp;
        events.insert(eventNewBar);
    }

    //testing
    while (events.size()) {
        auto event = *events.begin();
        events.erase(events.begin());
        Backtest::GetInstance().UpdateTimestamp(event.timestamp);
        if (event.type == Event::NEW_TRADE) {
            Backtest::GetInstance().AddTrade(event.newTrade);
        } else if (event.type == Event::NEW_BAR) {
            model.OnDataUpdate(event.newBar);
        } else if (event.type == Event::UPDATE_ORDER_BOOK) {
            Backtest::GetInstance().UpdateOrderBook(event.orderBook);
        } else if (event.type == Event::CANCEL_ORDER) {
            Backtest::GetInstance().CancelOrder();
            model.OnOrderUpdate(Backtest::CANCEL);
        } else if (event.type == Event::SUBMIT_ORDER) {
            Backtest::GetInstance().SubmitOrder(event.order);
            auto result = Backtest::GetInstance().CheckUpOrder(model);
            if (result != Backtest::NO_ACTION) {
                model.OnOrderUpdate(result);
            }
        } else if (event.type == Event::CHECK_UP) {
            auto result = Backtest::GetInstance().CheckUpOrder(model);
            if (result != Backtest::NO_ACTION) {
                model.OnOrderUpdate(result);
            }
            model.CheckUp();
        }
        auto &newEvents = Backtest::GetInstance().GetEvents();
        while (newEvents.size()) {
            events.insert(newEvents.front());
            newEvents.pop_front();
        }
    }
    log << "result: " << Backtest::GetInstance().GetResult() << std::endl;
    log.close();
    return 0;
}


