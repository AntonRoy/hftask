#pragma once

#include <iostream>
#include <vector>
#include <cmath>

#include "trade.h"

struct Bar {
    long double open, close, low, high, volume;
    long long timestamp;

    Bar() = default;

    Bar(const long double _open,
        const long double _close,
        const long double _low,
        const long double _high,
        const long double _volume,
        const long long _timestamp);

    void print() const;
};

std::vector<Bar> CreateBars(const std::vector<Trade>& trades, const long long timeframe);

