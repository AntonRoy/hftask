#include "trade.h"

Trade::Trade(const long double _tradePrice, const long double _tradeSize, const long long int _tradeTimestamp,
             const bool _isBuyerMaker)
        : tradePrice(_tradePrice), tradeSize(_tradeSize),
          tradeTimestamp(_tradeTimestamp), isBuyerMaker(_isBuyerMaker) {
}

long double Trade::GetTradePrice() const {
    return tradePrice;
}

long double Trade::GetTradeSize() const {
    return tradeSize;
}

long long Trade::GetTimestamp() const {
    return tradeTimestamp;
}

void Trade::print() const {
    std::cout << tradeTimestamp << " " << tradePrice << " " << tradeSize << " " << isBuyerMaker << "\n";
}

bool Trade::IsBuyerMaker() const {
    return isBuyerMaker;
}
