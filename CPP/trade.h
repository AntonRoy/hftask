#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>

class Trade {
public:
    Trade() = default;

    Trade(const long double _tradePrice, const long double _tradeSize,
          const long long _tradeTimestamp, const bool _isBuyerMaker);
    long double GetTradePrice() const;
    long double GetTradeSize() const;
    long long GetTimestamp() const;
    bool IsBuyerMaker() const;
    void print() const;
private:
    long double tradePrice, tradeSize;
    long long tradeTimestamp;
    bool isBuyerMaker;
};