#include "bar.h"

Bar::Bar(const long double _open,
         const long double _close,
         const long double _low,
         const long double _high,
         const long double _volume,
         const long long int _timestamp) : open(_open),
                                           close(_close),
                                           low(_low),
                                           high(_high),
                                           volume(_volume),
                                           timestamp(_timestamp) {
}

void Bar::print() const {
    std::cout << timestamp << " " << open << " " << close << " " <<
              low << " " << high << " " << volume << "\n";
}

std::vector<Bar> CreateBars(const std::vector<Trade> &trades, const long long int timeframe) {
    std::vector<Bar> bars;
    for (size_t i = 0; i < trades.size();) {
        size_t j = i;
        Bar bar(trades[i].GetTradePrice(),
                trades[i].GetTradePrice(),
                trades[i].GetTradePrice(),
                trades[i].GetTradePrice(),
                trades[i].GetTradeSize(),
                trades[i].GetTimestamp());
        while (j + 1 < trades.size()
               && (trades[j + 1].GetTimestamp() - trades[i].GetTimestamp()) < timeframe) {
            ++j;
            bar.close = trades[j].GetTradePrice();
            bar.timestamp = trades[j].GetTimestamp();
            bar.low = std::fmin(bar.low, trades[j].GetTradePrice());
            bar.high = std::fmax(bar.high, trades[j].GetTradePrice());
            bar.volume += trades[j].GetTradeSize();
        }
        bars.push_back(bar);
        i = j + 1;
    }
    return bars;
}