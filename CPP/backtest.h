#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <deque>

#include "trade.h"
#include "trade_manager.h"
#include "order.h"
#include "order_book.h"
#include "bar.h"
#include "position.h"
#include "event.h"
#include "model.h"

class Model;

class Backtest {
public:
    inline static std::string FILL = "fill",
            PARTIAL_FILL = "partial_fill",
            CANCEL = "cancel",
            REJECT = "reject",
            NO_ACTION = "no action";

    void init(const long long _limitOrderDelay,
              const long long _marketOrderDelay,
              const long long _orderCancelDelay,
              const long long _callFrequency,
              const long double _limitOrderCommission,
              const long double _marketOrderCommission);

    void UpdateTimestamp(const long long timestamp);

    long long Now() const;

    bool CancelOrder();

    void SubmitOrder(const std::optional<Order>& order);

    void UpdateOrderBook(const OrderBook& _orderBook);

    std::string CheckUpOrder(Model& model);

    void AddEvent(const Event& event);
    std::deque<Event>& GetEvents();
    void AddTrade(const Trade& trade);
    long double GetMarketPrice() const;
    const std::optional<Position>& GetPosition() const;
    const std::optional<Order>& GetOrder() const;
    long long GetLimitOrderDelay() const;
    long long GetMarketOrderDelay() const;
    long long GetOrderCancelDelay() const;
    long double GetResult() const;

    static Backtest& GetInstance();
private:
    std::deque<Event> events;
    std::optional<Order> order;
    std::optional<Position> position;
    OrderBook orderBook;
    std::vector<Trade> trades;
    long double limitOrderCommission, marketOrderCommission;
    long long currentTimestamp;
    long long limitOrderDelay, marketOrderDelay, orderCancelDelay;
    long long callFrequency;
    long double result;
};

void initBacktest(const std::string& filename);
