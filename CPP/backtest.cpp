#include "backtest.h"

void Backtest::init(const long long int _limitOrderDelay, const long long int _marketOrderDelay,
                    const long long int _orderCancelDelay, const long long int _callFrequency,
                    const long double _limitOrderCommission, const long double _marketOrderCommission) {
    limitOrderDelay = _limitOrderDelay;
    marketOrderDelay = _marketOrderDelay;
    orderCancelDelay = _orderCancelDelay;
    callFrequency = _callFrequency;
    limitOrderCommission = _limitOrderCommission;
    marketOrderCommission = _marketOrderCommission;
    result = 0;
}

void Backtest::UpdateTimestamp(const long long int timestamp) {
    currentTimestamp = timestamp;
}

long long Backtest::Now() const {
    return currentTimestamp;
}

bool Backtest::CancelOrder() {
    if (order) {
        order.reset();
        return true;
    }
    return false;
}

void Backtest::SubmitOrder(const std::optional<Order>& _order) {
    order = _order;
    if (!order->IsMarket()) {
        order->UpdateLimitQueue(
                orderBook.GetLimitQueue(order->GetPrice(),
                                        order->IsBuy()));
        for (size_t lastCurrentTrade = trades.size(); lastCurrentTrade >= 1 &&
                                                      trades[lastCurrentTrade - 1].GetTimestamp() >=
                                                      order->GetSubmitTimestamp();
             --lastCurrentTrade) {
            if (order->IsBuy() == trades[lastCurrentTrade - 1].IsBuyerMaker()) {
                order->UpdateLimitQueue(
                        order->GetLimitQueue() - trades[lastCurrentTrade - 1].GetTradeSize());
            }
        }
    }
}

std::string Backtest::CheckUpOrder(Model &model) {
    if (!order) {
        return Backtest::NO_ACTION;
    } else if (order->IsMarket()) {
        auto price = GetMarketPrice();
        if (order->IsBuy()) {
            long double commission = price * order->GetSize() * marketOrderCommission;
            if (price * order->GetSize() + commission > model.GetBalanceUsd()) {
                order.reset();
                return Backtest::REJECT;
            }
            if (!position) {
                position = Position(Now(), price, order->GetSize());
            } else {
                position->AddSize(order->GetSize());
            }
            long double delta = -order->GetSize() * price - commission;
            result += delta;
            model.AddBalanceUsd(delta);
            order.reset();
            return Backtest::FILL;
        } else {
            if (!position || position->GetSize() < order->GetSize()) {
                order.reset();
                return Backtest::REJECT;
            }
            long double commission = price * position->GetSize() * marketOrderCommission;
            long double delta = position->GetSize() * price - commission;
            result += delta;
            model.AddBalanceUsd(delta);
            position.reset();
            order.reset();
            return Backtest::FILL;
        }
    } else {
        long double canFill = std::fmin(order->GetSize(), -order->GetLimitQueue());
        if (canFill <= 0) {
            return Backtest::NO_ACTION;
        }
        long double commission = order->GetPrice() * canFill * limitOrderCommission;
        if (order->GetPrice() * canFill + commission > model.GetBalanceUsd()) {
            order.reset();
            return Backtest::REJECT;
        }
        if (order->IsBuy()) {
            if (!position) {
                position = Position(Now(), order->GetPrice(), canFill);
            } else {
                position->AddSize(canFill);
            }
            long double delta = -canFill * order->GetPrice() - commission;
            result += delta;
            model.AddBalanceUsd(delta);
        } else {
            if (!position) {
                return Backtest::REJECT;
            }
            position->AddSize(-canFill);
            long double delta = canFill * order->GetPrice() - commission;
            result += delta;
            model.AddBalanceUsd(delta);
        }
        order->AddSize(-canFill);
        if (!order->GetSize()) {
            order.reset();
            return Backtest::FILL;
        } else {
            return Backtest::PARTIAL_FILL;
        }
    }
}

void Backtest::AddEvent(const Event &event) {
    events.push_back(event);
}

std::deque<Event> &Backtest::GetEvents() {
    return events;
}

void Backtest::AddTrade(const Trade& trade) {
    trades.push_back(trade);
    if (order && !order->IsMarket() && order->IsBuy() != trade.IsBuyerMaker()) {
        order->UpdateLimitQueue(
                order->GetLimitQueue() - trade.GetTradeSize());
    }
}

long double Backtest::GetMarketPrice() const {
    return trades.back().GetTradePrice();
}

const std::optional<Position>& Backtest::GetPosition() const {
    return position;
}

const std::optional<Order>& Backtest::GetOrder() const {
    return order;
}

long long Backtest::GetLimitOrderDelay() const {
    return limitOrderDelay;
}

long long Backtest::GetMarketOrderDelay() const {
    return marketOrderDelay;
}

long long Backtest::GetOrderCancelDelay() const {
    return orderCancelDelay;
}

Backtest &Backtest::GetInstance() {
    static Backtest instance;
    return instance;
}

void Backtest::UpdateOrderBook(const OrderBook& _orderBook) {
    orderBook = _orderBook;
}

long double Backtest::GetResult() const {
    return result;
}

void initBacktest(const std::string& filename) {
    long long _limitOrderDelay, _marketOrderDelay, _orderCancelDelay;
    long long _callFrequency;
    long double _limitOrderCommission, _marketOrderCommission;
    std::ifstream file(filename);
    file >> _limitOrderDelay >> _marketOrderDelay >> _orderCancelDelay;
    file >> _callFrequency >> _limitOrderCommission >> _marketOrderCommission;
    Backtest::GetInstance().init(_limitOrderDelay, _marketOrderDelay, _orderCancelDelay,
                                 _callFrequency, _limitOrderCommission, _marketOrderCommission);
}