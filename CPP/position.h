#pragma once

#include <string>

class Position {
public:
    inline static std::size_t counter = 0;

    Position(long long _timestamp, long double _price, long double _size);

    long double GetPrice() const;

    long double GetSize() const;

    long long GetTimestamp() const;

    void AddSize(const double add);
private:
    std::size_t id;
    long long timestamp;
    long double price, size;
};