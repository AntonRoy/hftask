#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

#include "CPP/csv.h"
#include "CPP/order.h"

struct OrderBookRow {
    long double price, volume;
};

class OrderBook {
public:
    inline static const size_t orderBookSize = 50;

    OrderBook() = default;

    OrderBook(const CSVRow& row);

    long long GetTimestamp() const;

    long double GetLimitQueue(const long double price, bool isBuy) const;

    void print() const;

private:
    std::vector<OrderBookRow> asks, bids;
    long long timestamp;
};

std::vector<OrderBook> readOrderBooks(const std::string& filename);