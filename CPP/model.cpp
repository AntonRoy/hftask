#include "model.h"

Model::Model(const long double _maxUsdPosition,
             const long long int _deltaTime,
             const long long _endOfTradingPeriod,
             const long double _bestPriceDelta) {
    maxUsdPosition = _maxUsdPosition;
    deltaTime = _deltaTime;
    state = Model::BUY;
    balanceUsd = maxUsdPosition;
    endOfTradingPeriod = _endOfTradingPeriod;
    bestPriceDelta = _bestPriceDelta;
    sizeEps = 1e-6;
    priceEps = 1e-6;
}

bool Model::predict() {
    if (bars.size() < 20) {
        return false;
    }
    double zScore = GetZScore();
    return zScore < -1;
}

void Model::CancelOrder() {
    if (order) {
        Event eventCancelOrder;
        eventCancelOrder = Event();
        eventCancelOrder.type = Event::CANCEL_ORDER;
        eventCancelOrder.order = order;
        eventCancelOrder.timestamp = Backtest::GetInstance().Now() + Backtest::GetInstance().GetOrderCancelDelay();
        Backtest::GetInstance().AddEvent(eventCancelOrder);
    }
}

void Model::CheckUp() {
    order = Backtest::GetInstance().GetOrder();
    position = Backtest::GetInstance().GetPosition();
    if (order && order->IsBuy() && (Backtest::GetInstance().Now() - order->GetSubmitTimestamp()) > deltaTime) {
        CancelOrder();
    }
    if (position && Backtest::GetInstance().Now() >= endOfTradingPeriod - 50) {
        SubmitSell(Order::MARKET_ORDER);
    }
}

void Model::SubmitBuy() {
    if (!bars.size()) {
        return;
    }
    long double marketPrice = Backtest::GetInstance().GetMarketPrice();
    long double limitPrice = marketPrice - bestPriceDelta;
    long double orderSize = GetBalanceUsd() / limitPrice - sizeEps;
    long long submitTimestamp = Backtest::GetInstance().Now() + Backtest::GetInstance().GetLimitOrderDelay();
    order = Order(Order::LIMIT_ORDER, true, limitPrice, orderSize, submitTimestamp);
    Event eventSubmitOrder;
    eventSubmitOrder.type = Event::SUBMIT_ORDER;
    eventSubmitOrder.order = order;
    eventSubmitOrder.timestamp = submitTimestamp;
    Backtest::GetInstance().AddEvent(eventSubmitOrder);
    state = Model::BUY_SENT;
}

void Model::SubmitSell(const std::string &orderType) {
    if (!bars.size()) {
        return;
    }
    long double orderSize = position->GetSize();
    if (orderType == Order::MARKET_ORDER) {
        long long submitTimestamp = Backtest::GetInstance().Now() + Backtest::GetInstance().GetMarketOrderDelay();
        order = Order(Order::MARKET_ORDER, false, orderSize, submitTimestamp);
        Event eventSubmitOrder;
        eventSubmitOrder.type = Event::SUBMIT_ORDER;
        eventSubmitOrder.order = order;
        eventSubmitOrder.timestamp = submitTimestamp;
        Backtest::GetInstance().AddEvent(eventSubmitOrder);
        state = Model::SELL_SENT;
    } else {
        long double limitPrice = std::fmax(positionEntryAvgPrice + priceEps,
                                      Backtest::GetInstance().GetMarketPrice())
                                 + bestPriceDelta;
        long long submitTimestamp = Backtest::GetInstance().Now() + Backtest::GetInstance().GetLimitOrderDelay();
        order = Order(Order::LIMIT_ORDER, false, limitPrice, orderSize, submitTimestamp);
        Event eventSubmitOrder;
        eventSubmitOrder.type = Event::SUBMIT_ORDER;
        eventSubmitOrder.order = order;
        eventSubmitOrder.timestamp = submitTimestamp;
        Backtest::GetInstance().AddEvent(eventSubmitOrder);
        state = Model::SELL_SENT;
    }
}

void Model::OnOrderUpdate(const std::string &updateType) {
    position = Backtest::GetInstance().GetPosition();
    order = Backtest::GetInstance().GetOrder();
    if (updateType == Backtest::FILL) {
        order.reset();
        if (state == SELL_SENT) {
            position.reset();
            state = BUY;
        } else if (state == BUY_SENT) {
            position = Backtest::GetInstance().GetPosition();
            positionEntryAvgPrice = GetAvg();
            state = SELL;
            SubmitSell(Order::LIMIT_ORDER);
        }
    } else if (updateType == Backtest::PARTIAL_FILL) {
        positionEntryAvgPrice = GetAvg();
    } else if (updateType == Backtest::CANCEL || updateType == Backtest::REJECT) {
        order.reset();
        if (state == BUY_SENT) {
            if (position) {
                state = Model::SELL;
                SubmitSell(Order::LIMIT_ORDER);
            } else {
                state = Model::BUY;
            }
        } else if (state == SELL_SENT) {
            state = Model::SELL;
            if (rand() % 2) {
                SubmitSell(Order::MARKET_ORDER);
            }
        }
    }
}

void Model::OnDataUpdate(const Bar& bar) {
    bars.push_back(bar);
    if (state == Model::BUY && predict()) {
        SubmitBuy();
    } else if (state == Model::SELL && predictSel()) {
        SubmitSell(Order::LIMIT_ORDER);
    }
}

long double Model::GetBalanceUsd() const {
    return balanceUsd;
}

void Model::AddBalanceUsd(const double add) {
    balanceUsd += add;
}

long double Model::GetZScore() const {
    long double countBars = bars.size(), avg = GetAvg();
    long double volatility = 0;
    for (const auto& bar : bars) {
        volatility += (bar.close - avg) * (bar.close - avg);
    }
    volatility /= countBars;
    long double std = sqrt(volatility);
    return (bars.back().close - avg) / std;
}

long double Model::GetAvg() const {
    long double countBars = bars.size(), avg = 0;
    for (const auto& bar : bars) {
        avg += bar.close;
    }
    avg /= countBars;
    return avg;
}

bool Model::predictSel() {
    if (bars.size() < 20) {
        return false;
    }
    double zScore = GetZScore();
    return std::fabs(zScore) < 0.5;
}

