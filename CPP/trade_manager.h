#pragma once

#include "CPP/csv.h"
#include "CPP/trade.h"

class TradeManager {
public:
    TradeManager(const CSVRow& columns);
    Trade makeTrade(const CSVRow& row);
private:
    std::map<std::string, size_t> column2index;
};

std::vector<Trade> readTrades(const std::string& filename);