#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

class Order {
public:
    inline static const std::string MARKET_ORDER = "market_order";
    inline static const std::string LIMIT_ORDER = "limit_order";

    inline static size_t counter = 0;

    Order() = default;

    Order(const std::string& _orderType, const bool _isBuy,
          const long double _orderSize, const long long _submitTimestamp);
    Order(const std::string& _orderType, const bool _isBuy,
          const long double _orderPrice, const long double _orderSize,
          const long long _submitTimestamp);
    bool IsBuy() const;
    bool IsMarket() const;
    long long GetSubmitTimestamp() const;
    long double GetPrice() const;
    long double GetSize() const;
    void UpdateLimitQueue(const long double _limitQueue);
    long double GetLimitQueue() const;
    void AddSize(const long double add);
    void CanFullFill();
    bool IsCanFullFill() const;
private:
    size_t id;
    long long submitTimestamp;
    long double orderPrice, orderSize;
    long double limitQueue;
    std::string orderType;
    bool isBuy, canFullFill;
};