# HFTask

В data_engineering.ipynb находятся разборы стратегий, графики и описания

В /CPP находятся backtest, model и сопутсвующие классы. 

В model'и реализована Z-score стратегия.

В /Data находятся файлы backtest_params.txt и model_params.txt

В backtest_params.txt параметры идут в порядке: limitOrderDelay, marketOrderDelay, orderCancelDelay, callFrequency, limitOrderCommission, marketOrderCommission

В model_params.txt параметры идут в порядке: maxUsdPosition, timeframe, bestPriceDelta